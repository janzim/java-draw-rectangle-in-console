import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static boolean contains(final int[] arr, final int key) {
        return Arrays.stream(arr).anyMatch(i -> i == key);
    }

    /**
     * Read an integer input given a prompt
     * 
     * @param prompt the message to display to user when waiting for input
     * @return an integer
     */
    public static int readInput(String prompt) {
        Scanner sc = new Scanner(System.in);
        System.out.print(prompt);

        while (!sc.hasNextInt()) {
            if (sc.next().equals("q")) {
                System.out.println("Exiting...");
                System.exit(0);
            }

            System.out.println("Not a number!");
            System.out.print(prompt);
            sc.nextLine();
        }

        return sc.nextInt();
    }

    /**
     * Read input given a prompt and acceptable options.
     * 
     * @param prompt            the message to display to user when waiting for
     *                          input
     * @param acceptableChoices a list of choices
     * @return an integer
     */
    public static int readInput(String prompt, int... acceptableChoices) {
        int choice;

        do {
            choice = readInput(prompt);
        } while (!contains(acceptableChoices, choice));

        return choice;
    }

    /**
     * Generate a normal box using # given a width and height
     * 
     * @param width  the width of the box
     * @param height the height of the box
     */
    public static void generateBox(int width, int height) {
        for (int i = 0; i < height; i++) {
            if (i == 0 || i == height - 1) {
                System.out.println("#".repeat(width));
            } else {
                System.out.println("#" + " ".repeat(width - 2) + "#");
            }
        }
    }

    /**
     * Generate a nested box inside another box using #
     * 
     * @param width  width of outer box
     * @param height height of outer box
     */
    public static void generateDoubleBox(int width, int height) {
        for (int i = 0; i < height; i++) {
            if (i == 0 || i == height - 1) {
                System.out.println("#".repeat(width));
            } else if (width < 5 || height < 5 || i < 2 || i > height - 3) {
                System.out.println("#" + " ".repeat(width - 2) + "#");
            } else if (i == 2 || i == height - 3) {
                System.out.println("# " + "#".repeat(width - 4) + " #");
            } else if (i > 2 || i < height - 3) {
                System.out.println("# #" + " ".repeat(width - 6) + "# #");
            }
        }
    }

    public static void main(String[] args) {
        int option = readInput(
                "#".repeat(20) + "\n" + "# DRAW A RECTANGLE #\n" + "#".repeat(20) + "\n\n" + "Choose an option:\n"
                        + "1. Draw single rectangle (1)\n" + "2. Draw two nested rectangles (2)\n" + "Choice: ",
                1, 2);

        int width = readInput("Rectangle width: ");
        int height = readInput("Rectangle height: ");

        switch (option) {
            case 1:
                generateBox(width, height);
                break;
            case 2:
                generateDoubleBox(width, height);
                break;
            default:
                System.out.println("Something went wrong. Quitting...");
                break;
        }
    }
}